## Hinweise zum Antrag für die betriebliche Projektarbeit (IHK, Fachinformatiker*innen)

<span class="hidden-text" title="mastodonurl">https://bildung.social/@oerinformatik/110900934822752815</span>

<span class="hidden-text" title="arcticleurl">https://oer-informatik.de/ihk-projektantrag</span>

> **tl/dr;** _(ca. 7 min Lesezeit): Mit Einreichung der Projektanträge beginnt der zweite Prüfungsteil in den IT-Berufen. Bei den eingereichten Anträgen gibt es eine Reihe leicht vermeidbarer formeller und fachlicher Fehler, die hier gelistet werden. Außerdem gibt es einige subjektive Empfehlungen, die vielleicht helfen, den Projektantrag zu schärfen damit keine zeitfressenden Nachbesserungen nötig sind. Viele Punkte sind für alle Fachinformatiker*innen relevant, speziell zugeschnitten sind sie aber für die Fachrichtungen Anwendungsentwicklung und Daten- und Prozessanalyse._

### Rahmenbedingungen der Abschlussprüfung

Die verbindlichen Regeln für den Projektantrag sind in der Ausbildungsverordnung^[[Verordnung
über die Berufsausbildung zum Fachinformatiker und zur Fachinformatikerin
(Fachinformatikerausbildungsverordnung – FIAusbV)](https://www.bgbl.de/xaver/bgbl/start.xav?startbk=Bundesanzeiger_BGBl&jumpTo=bgbl120s0250.pdf) vom 28.2.2020, jeweils aktueller Link über [die Seite QualisNRW](https://www.berufsbildung.nrw.de/cms/bildungsgaenge-bildungsplaene/fachklassen-duales-system-anlage-a/berufe-a-bis-z/fachinformatikerin-fachinformatiker/bp-ao/bp-fachinformatiker.html)] festgelegt und werden von der IHK ggf. präzisiert. 

Viele IHKs haben eine Umsetzungsempfehlung veröffentlicht. Der aktueller Stand findet sich auf der jeweiligen IHK-Internetseite: für die IHK-OWL z.B. [hier unter IT-Berufe/Umsetzungsempfehlungen](https://www.ostwestfalen.ihk.de/pruefung/ausbildungspruefungen/berufe-mit-fachaufgabe-projekt-oder-dokumentation/) (Direktlink zum Dokument [hier](https://www.ostwestfalen.ihk.de/fileadmin/Dokumente/Berufliche_Bildung/Ausbildung/gewerblich-_technisch/DIHK-Umsetzungshilfe_IT-Berufe.pdf)). Daneben gibt es ggf. spezielle Regelungen, die im Schriftverkehr der IHK genannt werden - bitte diese Briefe im Vorfeld der Prüfung alle genau lesen!!!

Diese Dokumente stellen offizielle Empfehlungen bzw. verbindliche Regelungen dar, die vor dem Einstieg in das Prüfungsverfahren unbedingt durchgelesen und beachtet werden sollten. Die allermeisten Fehler lassen sich durch Durchlesen dieser Dokumente vermeiden.

Für den Projektantrag werden in der Umsetzungsempfehlung ab Seite 10 Kriterien genannt. Diese sollen hier durch einige subjektive Empfehlungen ergänzt werden. Da jede IHK und jeder Prüfungsausschuss jedoch andere Schwerpunkte setzt, stellt das nur eine unter vielen Meinungen dar.

### Versuch einer Checkliste für den Projektantrag

Der beste Weg ist, zunächst selbst einmal anhand der Umsetzungsempfehlung zu prüfen, ob alle Kriterien erfüllt wurden. Danach kann diese Checkliste herangezogen werden. Eine gute Idee ist es, den eigenen Antrag einer fachkundigen Person außerhalb des Betriebs zum Querlesen zu geben (z.B. innerhalb der Berufsschulklasse). Finden auch Dritte die in der Checkliste genannten Punkte und verstehen, was genau Ziel und Inhalt des Projektes ist?

Die meisten Fehler enthalten die Zeitpläne: die Arbeitspakete sind oft zu groß (am besten weniger als 8h), zu allgemein ("Implementierung") oder nicht hinreichend berufsrelevanten Phasen zugeordnet (keine Planung oder Qualitätssicherung ersichtlich).

#### Formalitäten:

- Der Antrag ist vollständig ausgefüllt (z.B. auch alle Adressen und Namen).

- Die Daten der Prüfungsteilnehmer*in (z.B. Ausbildungsberuf, Einsatzgebiet) sind angegeben.

- Die Angaben zu Ausbildungsbetrieb und betrieblich Betreuenden sind ausgefüllt.

#### Projektbezeichnung:

- Die Projektbezeichnung steht ggf. auf dem Berufsabschlusszeugnis – bitte mit Bedacht wählen!!!

- Die Projektbezeichnung und das Projektthema geben wesentliche Inhalte des Ausbildungsberufs wieder (passen in das Berufsbild). (Negativbeispiel: Bei "Abwicklung von Buchungen" fehlt der Informatik-Bezug)

- Die Projektbezeichnung ist spezifisch gewählt (Negativbeispiel: "Implementierung eines Webservice zur Datenspeicherung").

- Das Projekt ist ein betrieblicher Auftrag (keine "künstliche" Aufgabenstellung). Es sollte deutlich werden, inwieweit das Projekt in betriebliche Abläufe integriert ist (im Idealfall: der Unternehmensstrategie zuarbeitet).


#### Projektbeschreibung:

-	Das **fachliche Niveau** des Projekts ist für ein Abschlussprojekt **angemessen**. (Wenn das Projekt in dieser Form problemlos von Auszubildenden im 1. oder 2. Lehrjahr erstellt werden könnte, ist das Niveau vermutlich zu gering.)

-	Die **Projektbeschreibung** ist für fachkundige Dritte **verständlich / nachvollziehbar**.

-	Der **Umfang des Projekts ist** in der Projektbeschreibung klar **erkennbar**. (Es braucht kein betriebliches Expert*innenwissen, um abzuschätzen, was getan wird.)

-	Das Projekt ist in der **vorgegebenen Zeit durchführbar**. 

-	Das Projektergebnis ist dokumentierbar.

-	Der **Projektnutzen** ist erkennbar, die **Projektbegründung** daraus **nachvollziehbar** (Wirtschaftlichkeitsbetrachtung / Ziel des Auftrags).

#### Ausgangssituation

- Ist-Zustand: Das **Ausgangsproblem**, auf dem das Projekt beruht, ist **verständlich** dargestellt.

- Sofern bisherige **Geschäftsprozesse** optimiert / erweitert / verändert werden, sind diese kurz dargestellt.

#### Projektumfeld

-	Die wesentlichen Charakteristika des Projektumfelds, die zum Verständnis des Projektumfangs und des Anwendungsfalls erforderlich sind, sind kurz genannt. Dazu können beispielsweise gehören: Branche und Mitarbeiterzahl des Betriebs / der Abteilung / des Kunden. Wichtig ist hier v.a. der Teil des Betriebs, der in das Projekt hineinwirkt – bei größeren Betrieben ist ggf. die Abteilungsstruktur wichtiger als die Konzernstruktur.

-	Die (voraussichtlich) genutzten **Entwicklungswerkzeuge, Programmiersprachen** und Techniken bzw. Frameworks sind kurz genannt. (Wichtig zum Abschätzen des Umfangs: es macht eben einen Unterschied, ob mit Assembler oder einer LowCode-Plattform programmiert wird...)

-	Die IT-Infrastruktur ist kurz beschrieben, sofern sie zur Abschätzung des Projektumfangs bekannt sein sollte.

-	Die **Schnittstellen und Systemgrenzen** sind klar definiert. Mit welchen anderen Systemen / Akteuren gibt es Überschneidungen? Was ist Bestandteil des Projekts – und (sehr wichtig): was nicht mehr? 

-	Die Abgrenzung zu anderen Projektbeteiligten oder Teilprojekten ist nachvollziehbar: der Eigenanteil des Prüflings am Projekt ist klar herausgearbeitet, Fremdleistungen werden genannt.

#### Projektziel

- Die wesentlichen **funktionalen Anforderungen** (soweit schon bekannt) sind genannt.

- **Qualitätsanforderungen** ("nicht-funktionale Anforderungen") werden genannt - insbesondere Anforderungen mit Bezug auf **Sicherheit und Nachhaltigkeit** werden immer stärker gefordert! 

- Bereits bekannte Rahmenbedingungen werden genannt.

- Sofern es bereits eine Festlegung gibt, welche Anforderungen optional oder obligatorisch sind, wird dies genannt. Da dieses Projekt mit festem Stundenumfang realisiert werden muss, kann es sinnvoll sein, hier Variabilität im Projektumfang zu benennen.

#### Zeitplan (Projektphasen mit Zeitplanung und Stundenumfang):

- Das Projekt ist in **Projektphasen** gegliedert, deren Kernaufgaben als Arbeitspakete definiert und verständlich benannt sind.

- Die **berufsrelevanten Phasen** der Auftragsbearbeitung sind ausreichend identifiziert und zeitlich geplant. Beispielsweise sind Planung, Implementierung / Durchführung, Qualitätssicherung, Abnahme usw. als Phasen erkennbar (zugeschnitten auf das genutzte Vorgehensmodell, ggf. auch überschneidend).

- Die angegebene Aufteilung in Phasen und Arbeitspakete ist durchführbar und fachgerecht.

- Die Arbeitspakete und Phasen sind nicht allgemeingültig bezeichnet (z.B.: "Entwurf"), sondern **tragen konkrete projektspezifische Namen**, aus denen unmittelbar der Inhalt des Arbeitspakets hergeht.

- Die Struktur- und Zeitplanung ist realistisch, die **sachlogische Reihenfolge der Phasen und Arbeitspakete** ist gegeben. Zeitliche Abhängigkeiten sind erkennbar.

- Den definierten Phasen und Arbeitspakete sind **geplante Stundenumfänge** zugewiesen. Arbeitspakete sollten einen Arbeitstag (8h) sinnvollerweise nicht überschreiten, größere Arbeitspakete sollten mit Unterpunkten konkretisiert werden.

- Das Projekt sollte auf genau 80 Stunden (FIA) (40h bei FDP / FISy / FIV) geplant werden. Diese Stunden sollten komplett im Zeitplan ausgewiesen werden. Auf keinen Fall dürfen mehr Stunden verplant werden und es gibt keinen Grund dafür, weniger zu verplanen.

- Die Vorbereitung der Präsentation ist nicht in der Zeitplanung enthalten.

- Es werden **keine Pufferzeiten** im Zeitplan ausgewiesen. Puffer sind -falls nötig- in die jeweiligen Arbeitspakete einzukalkulieren, ohne diese im Zeitplan auszuweisen.

- Es wird davon ausgegangen, dass keine "Recherche" und "Einarbeitung" in Frameworks/Programme mehr nötig ist - sie sollten also nur in begründeten Ausnahmefällen im Zeitplan auftauchen. 

- Der Durchführungszeitraum beginnt erst nach der voraussichtlichen Genehmigung und endet vor der Abgabefrist.

- Der geplante Stundenumfang ist im Durchführungszeitraum abzuarbeiten 
(sinnvoll: Durchführungszeitraum mindestens 2 Wochen, Empfehlung die Abgabe auf dem Antrag auf den letzten Tag zu legen -Abgabe vorher ist in jedem Fall möglich).

#### Bestandteile der Projektarbeit:

- Die Dokumentationsmittel sind angemessen ausgewählt.

- Praxisübliche Unterlagen bzw. Unterlagen zur Kundendokumentation, die als Anlage beigefügt werden, sind genannt.

- Einige Prüfungsausschüsse legen fest, dass mehrere Dokumentationen abgegeben werden müssen (Projektdokumentation, Kundendokumentation, Entwicklerdokumentation) - hier bitte in die Unterlagen der jeweiligen IHK schauen!

#### Angabe von Hilfsmitteln:

-	Die Hilfsmittel für die Präsentation sind angemessen ausgewählt. (I.d.R.: Beamer)

#### Form, Grammatik und Rechtschreibung

Für den gesamten Prüfungsprozess gilt: Es ist hilfreich, nein: unerlässlich

- die Rechtschreibkorrektur des Textverarbeitungsprogramms zu nutzen und

- den Antrag von einer oder einer/ einem Dritten Korrektur lesen zu lassen.

### Abgabe des Projektantrags

Die obige Checkliste wurde abgearbeitet? 

Die Rechtschreibung und Grammatik wurde überprüft?

Ein/e Dritte hat den Antrag anhand der Checkliste gegengelesen?

Die Ausbilderin / der Ausbilder (oder der jeweils Auftraggebende) hat den Projektantrag gegengelesen und abgezeichnet?

Dann steht einer Abgabe nichts mehr im Weg. Viel Erfolg in der nächsten Phase der Projektbearbeitung!

### Links und weiterführende Informationen

- [Fachinformatikerausbildungsverordnung – FIAusbV](https://www.bgbl.de/xaver/bgbl/start.xav?startbk=Bundesanzeiger_BGBl&jumpTo=bgbl120s0250.pdf)

- [Umsetzungsempfehlung der IHK Ostwestfalen zu Bielefeld](https://www.ostwestfalen.ihk.de/fileadmin/Dokumente/Berufliche_Bildung/Ausbildung/gewerblich-_technisch/DIHK-Umsetzungshilfe_IT-Berufe.pdf)

